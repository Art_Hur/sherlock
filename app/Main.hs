module Main(main) where
import           Data.Wakfu.Exporter
import           Path
import           System.Environment        (getArgs)
import           System.IO                 (stdout)
import           System.Log.Formatter      (simpleLogFormatter)
import           System.Log.Handler        (setFormatter)
import           System.Log.Handler.Simple (streamHandler)
import qualified System.Log.Logger         as Log

main :: IO ()
main = do
    args <- getArgs
    if length args == 2 then do
        h <- streamHandler stdout Log.INFO >>= \lh -> return $
            setFormatter lh (simpleLogFormatter "[$time : $loggername : $prio] $msg")
        Log.updateGlobalLogger Log.rootLoggerName (Log.setLevel Log.INFO . Log.setHandlers [h])
        gameDir <- parseAbsDir $ head args
        outDir <- parseAbsDir $ args !! 1
        generateSourceCode gameDir outDir
    else putStrLn $ unlines
        [ "program takes 2 parameters:"
        , "absolute path to the main Wakfu directory"
        , "relative path to an output directory" ]

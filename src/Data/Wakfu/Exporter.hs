module Data.Wakfu.Exporter(generateSourceCode) where
import           Control.Monad          (forM_)
import           Control.Monad.IO.Class (liftIO)
import           Data.Algorithm.Diff
import           Data.Char              (toUpper)
import           Data.Fix               (Fix (..))
import           Data.List              (intercalate)
import           Data.Maybe             (fromMaybe)
import           Data.Monoid            ((<>))
import           Data.Wakfu.Inspect
import qualified Data.Yaml              as Yaml
import           Path
import           Path.IO                (doesFileExist)
import           Paths_sherlock
import qualified System.Log.Logger      as Log

loggerName :: String
loggerName = "wakfu-export"

generateSourceCode :: Path a Dir -> Path b Dir -> IO ()
generateSourceCode gamePath outputPath = do
    Log.infoM loggerName "inspecting in-game data structures"
    binariesName <- parseRelFile "game/lib/wakfu-client.jar"
    let binariesPath = gamePath </> binariesName
    exists <- doesFileExist binariesPath
    if not exists then
        error "couldn't find wakfu binary code files"
    else do
        bdatas <- inspectBinaryDataStructures binariesPath
        Log.infoM loggerName "writing scala sources"
        forM_ bdatas $ \bdata -> do
            let nm = dataName bdata
            structure <- liftIO $ loadStructureMembers nm
            writeFile (toFilePath outputPath <> nm <> ".scala") . toScalaDefinition $ withMembers structure bdata
    where
        withMembers structure bdata =
            fromMaybe bdata . flip fmap structure $ \mnms ->
                bdata { dataMembers = applyStructure (dataMembers bdata) mnms }

toScalaDefinition :: BinaryData -> String
toScalaDefinition (BinaryData bid n ms _) = unlines $
    [ "package com.github.wakfudecrypt.types.data"
    , ""
    , "import com.github.wakfudecrypt._"
    , ""
    ] <> toScalaCaseClass n ms <> foldl (collectComposites n) [] (fmap unFix ms) <>
    [ ""
    , "object " <> n <> " extends BinaryDataCompanion[" <> n <> "] {"
    , "  override val dataId = " <> show bid
    , "}"
    ]
    where
      toScalaCaseClass :: String -> [Member] -> [String]
      toScalaCaseClass n ms =
        [ "@BinaryDecoder"
        , "case class " <> n <> "("
        , intercalate ",\n" $ fmap (("  " <>) . toScalaMember n) ms
        , ")"
        ]

      toScalaMember :: String -> Member -> String
      toScalaMember pre m =
        let (Member name tpe) = unFix m in
        let fieldName = if name == "type" then "`type`" else name in
        fieldName <> ": " <> toScalaMemberType pre name tpe

      toScalaMemberType :: String -> String -> LabelledMemberType -> String
      toScalaMemberType _ _ Boolean = "Boolean"
      toScalaMemberType _ _ Int8 = "Byte"
      toScalaMemberType _ _ Int16 = "Short"
      toScalaMemberType _ _ Int32 = "Int"
      toScalaMemberType _ _ Int64 = "Long"
      toScalaMemberType _ _ Float32 = "Float"
      toScalaMemberType _ _ Float64 = "Double"
      toScalaMemberType _ _ Timestamp = "Long"
      toScalaMemberType _ _ Str = "String"
      toScalaMemberType pre n (Array m) =
        "Array[" <> toScalaMemberType pre n m <> "]"
      toScalaMemberType pre n (HashMap k v) =
        "Map[" <> toScalaMemberType pre n k <> ", " <> toScalaMemberType pre n v <> "]"
      toScalaMemberType pre n (Composite _) = compositeTypeName pre n

      collectComposites :: String -> [String] -> LabelledMember -> [String]
      collectComposites pre acc (Member n (Composite ms)) = renderComposite pre n ms <> acc
      collectComposites pre acc (Member n (Array (Composite ms))) = renderComposite pre n ms <> acc
      collectComposites pre acc (Member n (HashMap _ (Composite ms))) = renderComposite pre n ms <> acc
      collectComposites pre acc (Member n (HashMap _ (Array (Composite ms)))) = renderComposite pre n ms <> acc
      collectComposites _ acc _ = acc

      renderComposite pre n ms =
        let compositeName = compositeTypeName pre n in
        foldl (collectComposites compositeName) [] (fmap unFix ms) <> ("":toScalaCaseClass compositeName ms)

compositeTypeName :: String -> String -> String
compositeTypeName prefix fieldName =
    let (x: xs) = fieldName in prefix <> (toUpper x:xs)

loadStructureMembers :: String -> IO (Maybe [Member])
loadStructureMembers nm = do
    let pathStr = "structures/" <> nm <> ".yaml"
    dataFileName <- getDataFileName pathStr
    hasStructure <- doesFileExist =<< parseAbsFile dataFileName
    if hasStructure then
        Yaml.decodeFile dataFileName
    else return Nothing

applyStructure :: [Member] -> [Member] -> [Member]
applyStructure obfuscatedFields validFields =
    handleDiff $ getDiffBy (\a b -> extractType a == extractType b) obfuscatedFields validFields
    where
        handleDiff :: [Diff a] -> [a]
        handleDiff (First m:xs)  = m:handleDiff xs
        handleDiff (Both _ v:xs) = v:handleDiff xs
        handleDiff (Second _:xs) = handleDiff xs
        handleDiff []            = []
